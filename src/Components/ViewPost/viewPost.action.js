import { createRoutine } from 'redux-saga-routines';
import { all, call, put, takeLatest } from 'redux-saga/effects';

import api from '../../Config/api';

export const detailsPost = createRoutine('DETAILS_POST');
export const postComment = createRoutine('POST_COMMENT');

function* fetchPostDetailWorker({ payload }) {
  yield put(detailsPost.request());
  try {
    const res = yield call(() => api.descriptionOfPost(payload));
    if (res.data) {
      yield put(detailsPost.success(res.data));
    } else {
      yield put(detailsPost.failure('sore error'));
    }
  } catch (e) {
    yield put(detailsPost.failure(e));
  }
}

function* postCommentWorker({ payload }) {
  yield put(postComment.request());
  try {
    const res = yield call(() => api.createComment(payload));
    if (res) {
      yield put(postComment.success(res.data));
    } else {
      yield put(postComment.failure('sore error'));
    }
  } catch (e) {
    yield put(postComment.failure());
  }
}

export function* fetchDetailsWatcher() {
  yield all([takeLatest(postComment.TRIGGER, postCommentWorker)]);
  yield all([takeLatest(detailsPost.TRIGGER, fetchPostDetailWorker)]);
}
