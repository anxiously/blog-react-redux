import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ViewPost from './viewPost.component';
import { detailsPost, postComment } from './viewPost.action';

const mapStateToProps = state => ({
  status: state.details.detailsStatus,
  detailData: state.details.detailData,
  commentStatus: state.details.commentStatus,
  commentData: state.details.commentData,
  updateStatus: state.updatePost.status,
});

export default withRouter(
  connect(
    mapStateToProps,
    { detailsPost, postComment }
  )(ViewPost)
);
