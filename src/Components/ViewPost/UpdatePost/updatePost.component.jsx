import React, { Component, Fragment } from 'react';

class UpdatePost extends Component {
  state = {
    title: '',
    body: '',
  };
  componentDidMount() {
    this.setState({
      title: this.props.title,
      body: this.props.body,
    });
  }
  updatePost = ({ target: { name, value } }) => {
    this.setState(state => ({
      [name]: value,
      errors: {
        ...state.errors,
        [name]: '',
      },
    }));
  };
  update = () => {
    this.props.updatePost({ id: this.props.id, title: this.state.title, body: this.state.body });
  };
  render() {
    const { title, body } = this.state;
    return (
      <Fragment>
        <input onChange={this.updatePost} name="title" type="text" value={title} />
        <textarea onChange={this.updatePost} name="body" type="text" value={body} />
        <p onClick={this.update}>update</p>
      </Fragment>
    );
  }
}

export default UpdatePost;
