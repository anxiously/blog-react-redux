import { combineReducers } from 'redux';
import { handleActions } from 'redux-actions';

import * as actions from './updatePost.action';
const fetchingStatus = handleActions(
  {
    [actions.updatePost.REQUEST]() {
      return 'request';
    },
    [actions.updatePost.SUCCESS]() {
      return 'success';
    },
    [actions.updatePost.FAILURE]() {
      return 'failure';
    },
  },
  'none'
);

const updatePost = combineReducers({
  status: fetchingStatus,
});

export default updatePost;
