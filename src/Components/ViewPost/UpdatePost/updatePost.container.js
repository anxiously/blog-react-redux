import { connect } from 'react-redux';
import UpdatePost from './updatePost.component';
import { updatePost } from './updatePost.action';

const mapStateToProps = state => ({
  status: state.updatePost.status,
});

export default connect(
  mapStateToProps,
  { updatePost }
)(UpdatePost);
