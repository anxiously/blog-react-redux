import { createRoutine } from 'redux-saga-routines';
import { all, call, put, takeLatest } from 'redux-saga/effects';

import api from '../../../Config/api';

export const updatePost = createRoutine('UPDATE_POST');

function* updatePostWorker({ payload }) {
  yield put(updatePost.request());
  try {
    const res = yield call(() => api.updatePost(payload));
    if (res) {
      yield put(updatePost.success());
    } else {
      yield put(updatePost.failure('somethig went wrong on server, here should be message from server'));
    }
  } catch (e) {
    yield put(updatePost.failure(e));
  }
}

export function* updatePostWatcher() {
  yield all([takeLatest(updatePost.TRIGGER, updatePostWorker)]);
}
