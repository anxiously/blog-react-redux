import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import routes from '../../Config/routes';
import UpdatePost from './UpdatePost/updatePost.container';

class ViewPost extends Component {
  state = {
    comment: '',
    update: false,
  };
  componentDidMount() {
    this.props.detailsPost(this.props.match.params.id);
  }
  componentDidUpdate(prevProps) {
    if (prevProps.commentStatus !== this.props.commentStatus && this.props.commentStatus === 'success') {
      this.setState({
        comment: '',
      });
    }
    if (prevProps.updateStatus !== this.props.updateStatus && this.props.updateStatus === 'success') {
      this.props.detailsPost(this.props.match.params.id);
    }
  }
  handleChange = e => {
    this.setState({
      comment: e.target.value,
    });
  };
  updatePost = () => {
    this.setState({
      update: !this.state.update,
    });
  };
  sendComments = e => {
    this.props.postComment({ postId: this.props.detailData.id, body: this.state.comment });
  };
  render() {
    const { detailData, status } = this.props;
    return (
      <Fragment>
        {status === 'success' ? (
          <div>
            <Link to={routes().latestPosts}>Back</Link>
            <p>title: {detailData.title}</p>
            <p>body: {detailData.body}</p>
            <p>
              comments:
              {detailData.comments.map(comment => (
                <div key={comment.id}>{comment.body}</div>
              ))}
            </p>
            <form>
              <input value={this.state.comment} onChange={this.handleChange} type="text" placeholder="write coment" />
              <p onClick={this.sendComments}>comment</p>
            </form>
            <p onClick={this.updatePost}>Update Post</p>
            {this.state.update === true && (
              <UpdatePost id={detailData.id} title={detailData.title} body={detailData.body} />
            )}
          </div>
        ) : (
          <p>Loading...</p>
        )}
      </Fragment>
    );
  }
}

export default ViewPost;
