import { combineReducers } from 'redux';
import { handleActions } from 'redux-actions';

import * as actions from './viewPost.action';

const initialState = {
  title: '',
  body: '',
  id: '',
  comments: [],
};

const detailsStatus = handleActions(
  {
    [actions.detailsPost.REQUEST]() {
      return 'request';
    },
    [actions.detailsPost.SUCCESS]() {
      return 'success';
    },
    [actions.detailsPost.FAILURE]() {
      return 'failure';
    },
  },
  'none'
);

const commentStatus = handleActions(
  {
    [actions.postComment.REQUEST]() {
      return 'request';
    },
    [actions.postComment.SUCCESS]() {
      return 'success';
    },
    [actions.postComment.FAILURE]() {
      return 'failure';
    },
  },
  'none'
);

const detailData = handleActions(
  {
    [actions.detailsPost.SUCCESS](state, { payload }) {
      return payload;
    },
    [actions.postComment.SUCCESS](state, { payload }) {
      return { ...state, comments: [...state.comments, payload] };
    },
  },
  initialState
);

const details = combineReducers({
  detailData,
  detailsStatus,
  commentStatus,
});

export default details;
