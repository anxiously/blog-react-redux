import { connect } from 'react-redux';
import LatestPosts from './latestPosts.component';
import { fetchPosts, deletePost } from './latestPosts.action';

const mapStateToProps = state => ({
  posts: state.posts.data,
  deleteStatus: state.posts.deleteStatus,
  newPostStatus: state.newPost.status,
});

export default connect(
  mapStateToProps,
  { fetchPosts, deletePost }
)(LatestPosts);
