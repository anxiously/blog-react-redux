import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import routes from '../../Config/routes';
import NewPost from './NewPost/newPost.container';

class LatestPosts extends Component {
  state = {
    addStatus: false,
  };
  componentDidMount() {
    this.props.fetchPosts();
  }

  componentDidUpdate(prevProps) {
    if (
      (prevProps.deleteStatus !== this.props.deleteStatus && this.props.deleteStatus === 'success') ||
      (prevProps.newPostStatus !== this.props.newPostStatus && this.props.newPostStatus === 'success')
    ) {
      this.props.fetchPosts();
    }
  }
  addPost = () => {
    this.setState({
      addStatus: !this.state.addStatus,
    });
  };

  deletePost = id => e => {
    const result = confirm('are you sure?');
    if (result === true) {
      this.props.deletePost(id);
    }
  };
  render() {
    const { posts } = this.props;
    const titles = posts.map(item => (
      <div className="post" key={item.id}>
        <div name={item.id} onClick={this.showMore}>
          <div>{item.author}</div>
          <div>{item.title}</div>
          <div>{item.date}</div>
          <Link to={routes().viewPost + item.id}> More</Link>
          <p onClick={this.deletePost(item.id)}>delete post</p>
        </div>
      </div>
    ));
    return (
      <Fragment>
        {this.state.addStatus === true && <NewPost />}
        <p onClick={this.addPost}>add post</p>
        <div>{titles}</div>
      </Fragment>
    );
  }
}

export default LatestPosts;
