import { createRoutine } from 'redux-saga-routines';
import { all, call, put, takeLatest } from 'redux-saga/effects';

import api from '../../Config/api';

export const fetchPosts = createRoutine('FETCH_POSTS');
export const deletePost = createRoutine('DELETE_POST');

function* fetchPostsWorker() {
  yield put(fetchPosts.request());
  try {
    const res = yield call(() => api.listAllPosts());
    if (res.data) {
      yield put(fetchPosts.success(res.data));
    } else {
      yield put(fetchPosts.failure('sore error'));
    }
  } catch (e) {
    yield put(fetchPosts.failure(e));
  }
}

function* deletePostWorker({ payload }) {
  yield put(deletePost.request());
  try {
    const res = yield call(() => api.deletePost(payload));
    if (res) {
      yield put(deletePost.success());
    } else {
      yield put(deletePost.failure('error from server'));
    }
  } catch (e) {
    yield put(deletePost.failure('something went wrong'));
  }
}
export function* fetchPostsWatcher() {
  yield all([takeLatest(fetchPosts.TRIGGER, fetchPostsWorker)]);
  yield all([takeLatest(deletePost.TRIGGER, deletePostWorker)]);
}
