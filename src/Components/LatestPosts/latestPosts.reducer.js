import { combineReducers } from 'redux';
import { handleActions } from 'redux-actions';

import * as actions from './latestPosts.action';
const fetchingStatus = handleActions(
  {
    [actions.fetchPosts.REQUEST]() {
      return 'request';
    },
    [actions.fetchPosts.SUCCESS]() {
      return 'success';
    },
    [actions.fetchPosts.FAILURE]() {
      return 'failure';
    },
  },
  'none'
);
const deleteStatus = handleActions(
  {
    [actions.deletePost.REQUEST]() {
      return 'request';
    },
    [actions.deletePost.SUCCESS]() {
      return 'success';
    },
    [actions.deletePost.FAILURE]() {
      return 'failure';
    },
  },
  'none'
);

const postData = handleActions(
  {
    [actions.fetchPosts.SUCCESS](state, { payload }) {
      return payload;
    },
  },
  []
);

const posts = combineReducers({
  status: fetchingStatus,
  data: postData,
  deleteStatus,
});

export default posts;
