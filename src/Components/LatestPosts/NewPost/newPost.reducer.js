import { combineReducers } from 'redux';
import { handleActions } from 'redux-actions';

import * as actions from './newPost.action';

const fetchingStatus = handleActions(
  {
    [actions.createPost.REQUEST]() {
      return 'request';
    },
    [actions.createPost.SUCCESS]() {
      return 'success';
    },
    [actions.createPost.FAILURE]() {
      return 'failure';
    },
  },
  'none'
);

const newPost = combineReducers({
  status: fetchingStatus,
});

export default newPost;
