import React, { Component, Fragment } from 'react';

class NewPost extends Component {
  state = {
    title: '',
    body: '',
  };

  componentDidUpdate(prevProps) {
    if (prevProps.status !== this.props.status && this.props.status === 'success') {
      this.setState({
        title: '',
        body: '',
      });
    }
  }

  newPost = ({ target: { name, value } }) => {
    this.setState(state => ({
      [name]: value,
      errors: {
        ...state.errors,
        [name]: '',
      },
    }));
  };

  createNewPost = () => {
    this.props.createPost({ title: this.state.title, body: this.state.body });
  };
  render() {
    const { title, body } = this.state;
    return (
      <Fragment>
        <form>
          <input onChange={this.newPost} name="title" type="text" value={title} placeholder="enter title" />
          <textarea onChange={this.newPost} name="body" type="text" value={body} placeholder="enter post" />
          <p onClick={this.createNewPost}>Create Post</p>
        </form>
      </Fragment>
    );
  }
}
export default NewPost;
