import { createRoutine } from 'redux-saga-routines';
import { all, call, put, takeLatest } from 'redux-saga/effects';

import api from '../../../Config/api';

export const createPost = createRoutine('CREATE_POST');

function* createPostWorker({ payload }) {
  yield put(createPost.request());
  try {
    const res = yield call(() => api.createPost(payload));
    if (res) {
      yield put(createPost.success());
    } else {
      yield put(createPost.failure('something went wrong on server'));
    }
  } catch (e) {
    yield put(createPost.failure(e));
  }
}

export function* createPostWatcher() {
  yield all([takeLatest(createPost.TRIGGER, createPostWorker)]);
}
