import { connect } from 'react-redux';
import NewPost from './newPost.component';
import { createPost } from './newPost.action';

const mapStateToProps = state => ({
  status: state.newPost.status,
});

export default connect(
  mapStateToProps,
  { createPost }
)(NewPost);
