import React, { Component, Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';

import routes from '../Config/routes';
import LatestPosts from './LatestPosts/latestPosts.container';
import ViewPost from './ViewPost/viewPost.container';
import UpdatePost from './ViewPost/UpdatePost/updatePost.container';

import '../Css/App.css';

class App extends Component {
  render() {
    return (
      <Fragment>
        <Switch>
          <Route exact path={routes().latestPosts} component={LatestPosts} />
          <Route exact path={routes().viewPost + `:id`} component={ViewPost} />
          <Route exact path={routes().updatePost + `:id`} component={UpdatePost} />
        </Switch>
      </Fragment>
    );
  }
}

export default App;
