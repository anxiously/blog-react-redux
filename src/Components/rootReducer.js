import { combineReducers } from 'redux';
import posts from './LatestPosts/latestPosts.reducer';
import details from './ViewPost/viewPost.reducer';
import newPost from './LatestPosts/NewPost/newPost.reducer';
import updatePost from './ViewPost/UpdatePost/updatePost.reducer';

const appReducer = combineReducers({ posts, details, newPost, updatePost });

export default appReducer;
