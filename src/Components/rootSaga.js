import { all, fork } from 'redux-saga/effects';

import { fetchPostsWatcher } from './LatestPosts/latestPosts.action';
import { fetchDetailsWatcher } from './ViewPost/viewPost.action';
import { createPostWatcher } from './LatestPosts/NewPost/newPost.action';
import { updatePostWatcher } from './ViewPost/UpdatePost/updatePost.action';

export default function* rootSage() {
  yield all([fork(fetchPostsWatcher)]);
  yield all([fork(updatePostWatcher)]);
  yield all([fork(fetchDetailsWatcher)]);
  yield all([fork(createPostWatcher)]);
}
