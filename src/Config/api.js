import axios from 'axios';

const api = {
  //posts
  listAllPosts: () => axios.get('https://simple-blog-api.crew.red/posts'),
  createPost: data => axios.post('https://simple-blog-api.crew.red/posts', data),
  updatePost: data => axios.put(`https://simple-blog-api.crew.red/posts/${data.id}`, data),
  deletePost: id => axios.delete(`https://simple-blog-api.crew.red/posts/${id}`),
  descriptionOfPost: id => axios.get(`https://simple-blog-api.crew.red/posts/${id}?_embed=comments`),

  //comments
  createComment: data => axios.post('https://simple-blog-api.crew.red/comments', data),
};

export default api;
