export default params => {
  return {
    latestPosts: '/',
    viewPost: `/posts/`,
    updatePost: `/updatePost/`,
  };
};
